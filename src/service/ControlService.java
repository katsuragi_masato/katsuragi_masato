package service;

import static utils.DBUtils.*;

import java.sql.Connection;
import java.util.List;

import beans.Control;
import dao.ControlDao;


public class ControlService {

	public void register(Control control) {

		Connection connection = null;
		try {
			connection = getConnection();

			ControlDao controlDao = new ControlDao();
			controlDao.insert(connection, control);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	private static final int LIMIT_NUM = 1000;

	public List<Control> getControl() {

		Connection connection = null;
		try {
			connection = getConnection();

			ControlDao controlDao = new ControlDao();
			List<Control> ret = controlDao.getControl(connection, LIMIT_NUM);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	private void close(Connection connection) {
		// TODO 自動生成されたメソッド・スタブ
	}

public Control getControl(int userId) {

	Connection connection = null;
	try {
		connection = getConnection();

		ControlDao controlDao = new ControlDao();
		Control user = (Control) controlDao.getControl(connection, userId);

		commit(connection);

		return user;
	} catch (RuntimeException e) {
		rollback(connection);
		throw e;
	} catch (Error e) {
		rollback(connection);
		throw e;
	} finally {
		close(connection);
	}
}
}