package top;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Control;
import service.ControlService;

@WebServlet(urlPatterns = { "/control" })
public class control extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<Control> user = new ControlService().getControl();

		request.setAttribute("user", user);

		request.getRequestDispatcher("/control.jsp").forward(request, response);
	}
}


