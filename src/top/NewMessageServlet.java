package top;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.User;
import service.MessageServlet;

@WebServlet(urlPatterns = { "/post" })
public class NewMessageServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("post.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> messages = new ArrayList<String>();

        if (isValid(request, messages) == true) {

            User user = (User) session.getAttribute("loginUser");

            Message post = new Message();
            post.setUserId(user.getId());
            post.setText(request.getParameter("subject"));
            post.setText(request.getParameter("text"));
            post.setText(request.getParameter("category"));

            new MessageServlet().register(post);

            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("./");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

    	String subject = request.getParameter("subject");
        String message = request.getParameter("text");
        String category = request.getParameter("category");

        if (StringUtils.isEmpty(subject) == true) {
        	messages.add("件名を入力してください");
        }
        if (30 < subject.length()) {
        	messages.add("30文字以下で入力してください");
        }
        if (StringUtils.isEmpty(message) == true) {
        	messages.add("メッセージを入力してください");
        }
        if (140 < message.length()) {
        	messages.add("140文字以下で入力してください");
        }
        if (StringUtils.isEmpty(category) == true) {
        	messages.add("カテゴリーを入力してください");
        }
        if (10 < category.length()) {
        	messages.add("10文字以下で入力してください");
        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}