package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Control;
import exception.SQLRuntimeException;

public class ControlDao {

	public List<Control> getControl(Connection connection, int num) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("users.id as id, ");
			sql.append("users.login_id as login_id, ");
			sql.append("users.password as password, ");
			sql.append("users.name as name, ");
			sql.append("branch.name as branch, ");
			sql.append("position.name as position ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branch ");
			sql.append("ON users.branch = branch.id ");
			sql.append("INNER JOIN position ");
			sql.append("ON users.position = position.id ");
			sql.append("ORDER BY created_date");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<Control> ret = toControlList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private static  List<Control> toControlList(ResultSet rs)
			throws SQLException {

		List<Control> ret = new ArrayList<Control>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String login_id = rs.getString("login_id");
				String password = rs.getString("password");
				String name = rs.getString("name");
				String branch = rs.getString("branch");
				String position = rs.getString("position");

				Control control = new Control();
				control.setId(id);
				control.setLogin_id(login_id);
				control.setPassword(password);
				control.setName(name);
				control.setBranch(branch);
				control.setPosition(position);
				ret.add(control);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public void insert(Connection connection, Control post) {
		// TODO 自動生成されたメソッド・スタブ
	}
}