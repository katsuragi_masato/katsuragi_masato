<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>ユーザー管理画面</title>
</head>
<body>

	<br>

	<table>
		<tr>
			<th>ID</th>
			<th>ログインID</th>
			<th>名前</th>
			<th>支店</th>
			<th>役職</th>
			<th>編集</th>
		</tr>
		<c:forEach items="${user}" var="user">
					<tr>
						<td><span class="id"><c:out value="${user.id}" /></span></td>

						<td><span class="login_id"><c:out
									value="${user.login_id}" /></span></td>

						<td><span class="name"><c:out value="${user.name}" /></span></td>

						<td><span class="branch"><c:out
									value="${user.branch}" /></span></td>

						<td><span class="position"><c:out
									value="${user.position}" /></span></td>

						<form action="settings" method="get">
						<td><input type="submit" value="編集" /></td>
						</form></tr>

		</c:forEach>
	</table>
</body>
</html>
