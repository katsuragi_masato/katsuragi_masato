<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>${loginUser.login_id}の設定</title>
        <link href="css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main-contents">

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>

            <form action="settings" method="post"><br />
                <input name="id" value="${editUser.id}" id="id" type="hidden"/>
                <label for="login_id">アカウント名</label>
                <input name="login_id" value="${editUser.login_id}" id="login_id"/><br />

				<label for="name">名前</label>
                <input name="name" value="${editUser.name}"/> <br />

                <label for="password">パスワード</label>
                <input name="password" type="password" id="password" /><br />

                <label for="branch">支店</label>
                <select name="branch"  id="branch">
                <option value="1">本社</option>
	    		 <option value="2">支店A</option>
	   		 	 <option value="3">支店B</option>
	   		 	 <option value="4">支店C</option>
	   		 	 </select>
	   		 	 <script>
					document.getElementById('branch').value="${editUser.branch}";
				 </script>

                <label for="position">役職</label>
                <select name="position"  id="position">
                <option value="1">総務人事</option>
	    		 <option value="2">情報管理</option>
	   		 	 <option value="3">支店長</option>
	   		 	 <option value="4">社員</option>
	   		 	 </select>
	   		 	 <script>
					document.getElementById('id').value="${editUser.position}";
				 </script>

                <input type="submit" value="登録" /> <br />
                <a href="./">戻る</a>
            </form>
        </div>
    </body>
</html>