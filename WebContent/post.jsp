<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>新規投稿画面</title>
</head>
<body>
<div class="form-area">
				<form action="newMessage" method="post">
					新規投稿<br />
					<label for="subject">件名</label>
                	<input name="subject" id="subject"/> <br />
					<textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
					<br />
					<label for="category">カテゴリー</label>
                	<input name="category" id="category"/> <br />
					<input type="submit" value="投稿" />
			</form>
	</div>
</body>
</html>